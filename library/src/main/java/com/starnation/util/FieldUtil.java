package com.starnation.util;

/*
 * @author vkwofm
 * @since 2016. 11. 4.
*/

import com.starnation.util.validator.ArrayValidator;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class FieldUtil {

    //======================================================================
    // Public Methods
    //======================================================================

    public static Type getActualTypeArguments(Field field, int position) {
        Type[] types = getActualTypeArguments(field);
        if (ArrayValidator.isValid(types, position) == true) {
            return types[position];
        }

        return null;
    }

    public static Type[] getActualTypeArguments(Field field) {
        ParameterizedType type = getGenericParameterizedType(field);
        if (type != null) {
            return type.getActualTypeArguments();
        }

        return null;
    }

    public static ParameterizedType getGenericParameterizedType(Field field) {
        return getGenericParameterizedTypeInternal(field);
    }

    //======================================================================
    // Private Methods
    //======================================================================

    static ParameterizedType getGenericParameterizedTypeInternal(Field field) {
        return (ParameterizedType) field.getGenericType();
    }
}
