package com.starnation.util;

/**
 * 계산 관련
 *
 * @author lsh
 * @since 2016. 5. 5.
 */
public final class CalculateUtil {

    /**
     * @param current 현재 정수 값
     * @param start   Loop 시작 값
     * @param end     Lop 종료 값
     *
     * @return start >  current && current <= end
     */
    public static int upLoop(int current, int start, int end) {
        if (++current > end) {
            return start;
        } else {
            return current;
        }
    }

    /**
     * @param current 현재 정수 값
     * @param start   Loop 시작 값
     * @param end     Lop 종료 값
     *
     * @return start >  current && current <= end
     */
    public static int downLoop(int current, int start, int end) {
        if (--current < start) {
            return end;
        } else {
            return current;
        }
    }

    public static int upCount(int num, int en) {
        if (++num > en) return en;
        else return num;
    }

    public static int downCount(int num, int sn) {
        if (--num < sn) return sn;
        else return num;
    }

    public static float percent(float value, float valueMin, float min, float max) {
        return ((value - valueMin) * max) / (max - min);
    }
}
