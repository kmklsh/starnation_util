package com.starnation.util;

import android.util.Patterns;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * @author lsh
 * @since 14. 12. 10.
*/
public final class PatternUtil {

    //======================================================================
    // Constants
    //======================================================================

    private final static String E_MAIL = "^[_a-zA-Z0-9-\\.]+@[\\.a-zA-Z0-9-]+\\.[a-zA-Z]+$";

    private final static String ALPHA_NUM = "^[a-zA-Z0-9]+$";

    public final static String NUMBER = "\"^[0-9]+$\"";

    public final static Pattern LINE_SEPARATOR = Pattern.compile("\n|\r");

    public static final String PATTERN_SPECIAL_LETTERS = "\\p{Punct}";

    public final static String PATTERN_URL_SCHEME = "^(https?):\\/\\/";

    @SuppressWarnings("MalformedRegex")
    public static Pattern KOREA_LANGUAGE_PATTERN = Pattern.compile("[\\x{ac00}-\\x{d7af}]|[ㄱ-ㅎㅏ-ㅣ가-힣]");

    public final static Pattern URL = Pattern.compile("^(https?):\\/\\/([^:\\/\\s]+)(:([^\\/]*))?((\\/[^\\s/\\/]+)*)?\\/([^#\\s\\?]*)(\\?([^#\\s]*))?(#(\\w*))?$");

    //======================================================================
    // Public Methods
    //======================================================================

    public static boolean isSuffixMatchHTTP(String url) {
        return Pattern.compile(PATTERN_URL_SCHEME).matcher(url).matches();
    }

    public static boolean isEmail(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static String toEmailSuffix(String email) {
        return email.replaceAll("[_a-zA-Z0-9-\\.]+@", "");
    }

    public static boolean isPhoneNumber(String phoneNum) {
        return Patterns.PHONE.matcher(phoneNum).matches();
    }

    public static String toPhoneNumberSuffix(String phoneNum) {
        return "";//phoneNum.replaceAll("^+8201{5}|+821{4}|8201{4}|821{3}", "01");
    }

    public static boolean isHttpStartWidth(String scheme) {
        return Pattern.matches("(?:http|https|rtsp):\\/\\/[A-Za-z0-9.:]+", scheme);
    }

    public static boolean isAlphaNum(String alphaNum) {
        return Pattern.matches(ALPHA_NUM, alphaNum);
    }

    public static boolean isWebUrl(CharSequence url) {
        return Patterns.WEB_URL.matcher(url).matches();
    }

    public static boolean isLineSeparator(String value) {
        return LINE_SEPARATOR.matcher(value).matches();
    }

    public static String toLineSeparatorEmpty(String value) {
        return value.replaceAll("\n|\r", "");
    }

    public static String toFindValue(Matcher matcher) {
        if (matcher != null) {
            StringBuilder stringBuffer = new StringBuilder();

            for (int i = 0; i < matcher.groupCount(); i++) {
                stringBuffer.append(matcher.group(i));
            }
            return stringBuffer.toString();
        }
        return null;
    }

    public static String getMatcherKoreaLanguageEncoding(String input, String charsetName) {
        if (StringUtil.isEmpty(input) == true) {
            return input;
        }
        Matcher matcher = KOREA_LANGUAGE_PATTERN.matcher(input);

        while (matcher.find()) {
            try {
                input = input.replaceAll(matcher.group(), URLEncoder.encode(matcher.group(), charsetName));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return input;
    }

    public static boolean isPatternSpecialLetters(String value) {
        return StringUtil.isEmpty(value) == false && Pattern.matches(PATTERN_SPECIAL_LETTERS, value);
    }
}
