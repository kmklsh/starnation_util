package com.starnation.util.validator;

/*
 * @author lsh
 * @since 2016. 2. 25.
*/
public final class ArrayValidator {

    //======================================================================
    // Constants
    //======================================================================

    private static final int NO_INDEX = -1;

    //======================================================================
    // Public Methods
    //======================================================================

    public static boolean isValid(String[] array) {
        return isValidInternal(array, 0);
    }

    public static boolean isValid(String[] array, int index) {
        return isValidInternal(array, index);
    }

    public static boolean isValid(Object[] array) {
        return isValidInternal(array, 0);
    }

    public static boolean isValid(Object[] array, int index) {
        return isValidInternal(array, index);
    }

    //======================================================================
    // Private Methods
    //======================================================================

    public static boolean isValidInternal(Object[] array, int index) {
        return index > NO_INDEX && array != null && array.length > index;
    }
}
