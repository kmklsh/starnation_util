package com.starnation.util.validator;

import com.starnation.util.StringUtil;

/*
 * @author lsh
 * @since 2016. 2. 26.
*/
public final class IsbnValidator {

    public static void valid(String isbn) throws Exception {
        if (StringUtil.isEmpty(isbn) == true) {
            throw new NullPointerException("isbn null");
        }

        //remove any hyphens
        isbn = isbn.replaceAll("-", "");

        //must be a 13 digit ISBN
        if ((isbn.length() == 13 || isbn.length() == 10) == false) {
            throw new InvalidException("Isbn InvalidException");
        }

        try {
            int tot = 0;
            if (isbn.length() == 10) {
                for ( int i = 0; i < 9; i++ )
                {
                    int digit = Integer.parseInt( isbn.substring( i, i + 1 ) );
                    tot += ((10 - i) * digit);
                }

                int checksum = (11 - (tot % 11)) % 11 ;
                if ( checksum == 10 )
                {
                    checksum = 0;
                }

                boolean valid = checksum == Integer.parseInt(isbn.substring(9));
                if (valid == false) {
                    throw new InvalidException("Isbn InvalidException");
                }
            } else if (isbn.length() == 13){
                for (int i = 0; i < 12; i++) {
                    int digit = Integer.parseInt(isbn.substring(i, i + 1));
                    tot += (i % 2 == 0) ? digit : digit * 3;
                }

                //checksum must be 0-9. If calculated as 10 then = 0
                int checksum = 10 - (tot % 10);
                if (checksum == 10) {
                    checksum = 0;
                }

                boolean valid = checksum == Integer.parseInt(isbn.substring(12));
                if (valid == false) {
                    throw new InvalidException("Isbn InvalidException");
                }
            }
        } catch (NumberFormatException nfe) {
            throw new InvalidException(nfe.getMessage());
        }
    }
}
