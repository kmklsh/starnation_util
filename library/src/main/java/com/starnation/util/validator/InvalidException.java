package com.starnation.util.validator;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

/*
 * @author lsh
 * @since 2016. 2. 26.
*/
public class InvalidException extends Exception {

    @StringRes
    int mDetailMessage = -1;

    public InvalidException(String detailMessage) {
        super(detailMessage);
    }

    public InvalidException(@NonNull Context context, @StringRes int detailMessage) {
        super(context.getString(detailMessage));
    }

    public InvalidException(@StringRes int detailMessage) {
        mDetailMessage = detailMessage;
    }

    public static String getErrorMessage(@NonNull Context context, InvalidException e) {
        if (e.mDetailMessage != -1) {
            return e.asDetailMessage(context);
        }
        return e.getMessage();
    }

    public String asDetailMessage(@NonNull Context context) {
        if (mDetailMessage != -1) {
            return context.getString(mDetailMessage);
        }
        return "";
    }
}
